import java.util.Scanner;

public class HelloWorld {

    static void order(String food) {
        System.out.print("you have ordered " + food + " Thank you!");
    }
    public static void main(String[] args){
        System.out.print("What world you like to order:\n1. Tempura\n2. Ramen\n3. udon\nYour order: ");
        Scanner userIn = new Scanner(System.in);
        int num = userIn.nextInt();
        userIn.close();
        if(num==1){
            order("Tempura");
        }
        else if(num==2){
            order("Ramen");
        }
        else if(num==3){
            order("Udon");
        }
    }

}

