import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodGUI {
    private JPanel root;
    private JButton tendonButton;
    private JButton ramenButton;
    private JButton udonButton;
    private JButton oyakodonButton;
    private JButton curryButton;
    private JButton sobaButton;
    private JButton checkOutButton;
    private JTextArea textArea1;
    private JTextArea textArea2;
    int sum = 0;

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodGUI");
        frame.setContentPane(new FoodGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }


    void order(String food, int price) {
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if (confirmation == 0) {
            int largeconfirmation = JOptionPane.showConfirmDialog(null,
                    "Would you like a large portion for free?",
                    "Order Confirmation",
                    JOptionPane.YES_NO_OPTION);
            String currentText = textArea1.getText();
            if (largeconfirmation == 0){
                JOptionPane.showMessageDialog(null, "Thank you for ordering large size " + food + "! It will be served as soon as possible.");
            }
            else{
                JOptionPane.showMessageDialog(null, "Thank you for ordering normal size " + food + "! It will be served as soon as possible.");
            }



            textArea1.setText(currentText + food + " " + price + "yen" + "\n");

            sum += price;
            textArea2.setText("total "+ sum + "yen");
        }
    }

    public FoodGUI() {
        tendonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tendon",800);
            }
        });


        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen",700);
            }
        });

        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon",500);
            }
        });

        oyakodonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Oyakodon",300);
            }
        });

        curryButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Curry",600);
            }
        });
        sobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Soba",900);
            }
        });

        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null ,"Would you like to check out ?" ,
                        "Order Confirmation",JOptionPane.YES_NO_OPTION );
                if(confirmation == 0 ) {
                    JOptionPane.showMessageDialog(null, "Thank you. The total price is " + sum + " yen");
                    textArea1.setText("");
                    textArea2.setText("0");
                    sum=0;
                }
            }
        });
    }
}